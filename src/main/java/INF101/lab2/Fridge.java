package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;
    ArrayList<FridgeItem> fridge_items = new ArrayList<>();

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridge_items.size() < totalSize()) {
            fridge_items.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public int nItemsInFridge() {
        return fridge_items.size();
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridge_items.size() >= 1) {
            fridge_items.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }        
    }

    @Override
    public void emptyFridge() {
        fridge_items.clear();    
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> nogoFood = new ArrayList<>();

        for (int i = 0; i < nItemsInFridge(); i++) {
            FridgeItem susFood = fridge_items.get(i);
            if (susFood.hasExpired()) {
                nogoFood.add(susFood);
                takeOut(susFood);
                i--;
            }
        }

        return nogoFood;
    }
}
